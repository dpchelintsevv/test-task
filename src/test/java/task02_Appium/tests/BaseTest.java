//package task02_Appium.tests;
//
//import io.appium.java_client.AppiumDriver;
//import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.ios.IOSDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
//
//import java.net.URL;
//
//public abstract class BaseTest {
//    private static final String APPIUM_SERVER_URL = "http://localhost:4723/wd/hub"; // Appium server URL
//
//    public static AppiumDriver initializeDriver(String platform) {
//        AppiumDriver driver;
//
//        DesiredCapabilities capabilities = new DesiredCapabilities();
//
//        try {
//            if (platform.equalsIgnoreCase("iOS")) {
//                driver = new IOSDriver(new URL(APPIUM_SERVER_URL), capabilities);
//            } else if (platform.equalsIgnoreCase("Android")) {
//                driver = new AndroidDriver(new URL(APPIUM_SERVER_URL), capabilities);
//            } else {
//                throw new IllegalArgumentException("Unsupported platform: " + platform);
//            }
//        } catch (Exception e) {
//            throw new RuntimeException("Failed to initialize driver: " + e.getMessage());
//        }
//
//        return driver;
//    }
//}
