package task01_Selenium.tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import task01_Selenium.models.User;
import task01_Selenium.models.UserBuilder;
import task01_Selenium.pages.LoginPage;

import java.io.IOException;

public class SwagLabTest extends BaseTest {

    LoginPage loginPage = new LoginPage(driver);
    User user;

    @BeforeEach
    void setup() throws IOException {
        user = UserBuilder.generateRandomUser();
    }

    @Test
    void testCheckout() {
         loginPage
                .login(user.getUsername(), user.getPassword())
                .selectItem("Sauce Labs Backpack")
                .selectItem("Sauce Labs Fleece Jacket")
                .selectItem("Sauce Labs Onesie")
                .clickOnCartLink()
                .removeItem("Sauce Labs Onesie")
                .clickOnCheckoutButton()
                .enterCheckoutData(user)
                        .verifyTotalPrice("86.38")
                .clickOnFinishButton()
                .verifyCheckoutCompleted();
    }
}
