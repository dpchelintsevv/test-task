package task03_Java.subtask_01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class Task {

    //Obtain a list of products belonging to category “Books” with price > 100 using
    //stream.

    public static void main(String[] args) {
        List<Product> products = new ArrayList<>();

        products.add(new Product(1L, "Product 1", "Books", 105.0, new HashSet<>()));
        products.add(new Product(2L, "Product 2", "Food", 20.0, new HashSet<>()));
        products.add(new Product(3L, "Product 3", "Sport", 15.0, new HashSet<>()));
        products.add(new Product(4L, "Product 4", "Books", 215.0, new HashSet<>()));
        products.add(new Product(5L, "Product 5", "Books", 15.0, new HashSet<>()));
        products.add(new Product(6L, "Product 6", "Sport", 315.0, new HashSet<>()));
        products.add(new Product(7L, "Product 7", "Food", 415.0, new HashSet<>()));

        List<Product> filteredProducts = products.stream()
                .filter(x -> x.getCategory().equals("Books") && x.getPrice() > 100)
                .collect(Collectors.toList());
    }
}
