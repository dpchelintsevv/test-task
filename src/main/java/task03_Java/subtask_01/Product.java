package task03_Java.subtask_01;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class Product {
    private Long id;
    private String name;
    private String category;
    private Double price;
    private Set<Order> orders;
}
