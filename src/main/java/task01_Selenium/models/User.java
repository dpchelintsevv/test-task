package task01_Selenium.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Data
public class User {
    private String firstname;
    private String lastname;
    private String zip;
    private String username;
    private String password;
}
