package task01_Selenium.models;

import com.github.javafaker.Faker;
import lombok.Data;
import task01_Selenium.utils.Utils;

import java.io.IOException;

@Data
public class UserBuilder {

    static Faker faker = new Faker();

    public static User generateRandomUser() throws IOException {
        String firstname = faker.name().firstName();
        String lastname = faker.name().lastName();
        String zip = faker.address().zipCode();
        String username = Utils.getUsername();
        String password = Utils.getPassword();

        return new User(firstname, lastname, zip, username, password);
    }
}
