package task01_Selenium.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Utils {

    private static final String PROPERTIES_FILE_PATH = "src/test/resources/loginData.properties";

    private static Properties loadProperties() throws IOException {
        Properties properties = new Properties();
        FileInputStream inputStream = new FileInputStream(PROPERTIES_FILE_PATH);
        properties.load(inputStream);
        inputStream.close();
        return properties;
    }

    public static String getUsername() throws IOException {
        Properties properties = loadProperties();
        return properties.getProperty("username");
    }

    public static String getPassword() throws IOException {
        Properties properties = loadProperties();
        return properties.getProperty("password");
    }
}
