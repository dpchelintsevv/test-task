package task01_Selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Optional;

public class ProductsPage extends BasePage{
    @FindBy(className = "inventory_item")
    private List<WebElement> items;

    @FindBy(className = "shopping_cart_link")
    private WebElement cartLink;

    public ProductsPage(WebDriver driver) {
        super(driver);
    }

    public ProductsPage selectItem(String itemName) {
        Optional<WebElement> itemToSelect = items.stream()
                .filter(item -> item.findElement(By.cssSelector(".inventory_item_name"))
                        .getText().equals(itemName))
                .findFirst();

        if (itemToSelect.isPresent()) {
            WebElement addToCartButton = itemToSelect.get().findElement(By.cssSelector("[id*='add-to-cart']"));
            addToCartButton.click();
        } else {
            throw new NoSuchElementException("Item with name '" + itemName + "' not found");
        }
            return this;
    }

    public CartPage clickOnCartLink(){
        cartLink.click();
        return new CartPage(driver);
    }
}
