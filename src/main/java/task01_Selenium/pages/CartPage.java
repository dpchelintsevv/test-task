package task01_Selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Optional;

public class CartPage extends BasePage {
    @FindBy(className = "cart_item")
    private List<WebElement> cartItems;

    @FindBy(className = "inventory_item_price")
    private List<WebElement> priceElements;

    @FindBy(id = "checkout")
    private WebElement checkoutButton;


    public CartPage(WebDriver driver) {
        super(driver);
    }

    public CartPage removeItem(String itemName) {
        Optional<WebElement> itemToSelect = cartItems.stream()
                .filter(item -> item.findElement(By.cssSelector(".inventory_item_name"))
                        .getText().equals(itemName))
                .findFirst();

        if (itemToSelect.isPresent()) {
            WebElement addToCartButton = itemToSelect.get().findElement(By.cssSelector("[id*='remove-sauce-labs']"));
            addToCartButton.click();
        } else {
            throw new NoSuchElementException("Item with name '" + itemName + "' not found");
        }
        return this;
    }

    public CheckoutPage clickOnCheckoutButton(){
        checkoutButton.click();
        return new CheckoutPage(driver);
    }
}
