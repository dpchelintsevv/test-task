# Test Task Solution

This repository contains my solution to a test task that consists of three parts: Selenium, Appium, and Java.

## Selenium Task

The solution for the Selenium task is located in the following directories:
- `src/main/java/task01_Selenium`: Contains the implementation of framework-related functionality.
- `src/test/java/task01_Selenium/tests`: Contains the test cases for the Selenium task.

To run the Selenium tests, use the following command:

gradle clean test


## Appium Task

The solution for the Appium task is located in the following directories:
- `src/main/java/task02_Appium/pages`: Contains the page objects for the Appium task.
- `src/test/java/task02_Appium/tests`: Contains the test cases for the Appium task.

Please note: Code is commented because of some compatibility issues between appium
and selenium drivers. You also need to uncomment line 15 in build.gradle file.

## Java Task

The solution for the Java task is located in the following directory:
- `src/main/java/task03_Java`: Contains the implementation for the Java task.

For any questions or clarifications, please contact me.

Thank you!
